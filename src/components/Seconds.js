import React, {useEffect, useState} from "react";

const Seconds = () =>{
    const [seconds, setSeconds] = useState(0);

    useEffect(() => {
        const timer = setInterval(() => {
            setSeconds(seconds => seconds+1);
        }, 5000);

        return () => {
            clearInterval(timer);
        }
    }, [])

    useEffect(() => {
       console.log("Aparece toda vez");

       return () => {
           console.log("chamado antes do render do proximo update")
       }
    }, []);

    return <span>Você gastou {seconds} segundos</span>
}

export default Seconds;
