import React, {useEffect, useState} from "react";
import axios from "axios";
import List from "./List";

const SearchWiki = () => {
    const [value, setValue] = useState("");
    const [result, setResult] = useState([]);

    useEffect(() => {
        let timerId = null;

        if(value) {
            timerId = setTimeout(async () => {
                const { data } = await axios.get("http://pt.wikipedia.org/w/api.php", {
                    params: {
                        action: "query",
                        list: "search",
                        origin: "*",
                        format: "json",
                        srsearch: value
                    }
                });

                setResult(data.query.search);
            }, 1000);
        }

        return () => {
            clearTimeout(timerId);
        }
    }, [value]);

    return (
        <>
            <form className="ui form">
                <input
                    type="text"
                    placeholder="Search Wikipedia..."
                    value={value}
                    onChange={(e) => setValue(e.target.value)}
                ></input>
            </form>
            <List results={result} />
        </>
    );
}

export default SearchWiki;
