import React, {useEffect} from "react";

const TestUseEffect= () => {
    let testVar;

    useEffect(() => {
        console.log("eae");
    }, [testVar])

    return <h1>Test</h1>
}

export default TestUseEffect;
