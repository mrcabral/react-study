import React from "react";

const List = ({ results }) => {
    const renderedResults = results.map((item) => {
        return (
            <div key={item.pageid} className="ui segment">
                <h2>
                    <a
                        href={"http://pt.wikipedia.org?curid="+item.pageid}
                        className="header"
                        target="_blank"
                        rel="noreferrer"
                    >{item.title}</a>
                </h2>
                <p dangerouslySetInnerHTML={{ __html: item.snippet}}></p>
            </div>
        );
    })

    return renderedResults;
}

export default List;
