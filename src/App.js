import React from "react";
import SearchWiki from "./components/SearchWiki";

class App extends React.Component {
    render() {
        return (
            <div className="ui grid container center aligned">
                <div className="column eight wide">
                    <SearchWiki />
                </div>

            </div>
        );
    };
}

export default App;
